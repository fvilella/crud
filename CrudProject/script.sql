CREATE TABLE departamento (
	id int(11) unique NOT NULL auto_increment,
	nome varchar(55) NOT NULL,
	descricao varchar(255),
	PRIMARY KEY (id)
);

CREATE TABLE empregado (
	id int(11) unique NOT NULL auto_increment,
	nome varchar(45) NOT NULL,
	id_departamento int(11) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_departamento) REFERENCES departamento(id)
);