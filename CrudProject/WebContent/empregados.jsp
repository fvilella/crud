<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Empregado</title>
</head>
<body>
 
<h1>Empregados</h1>
<br />
<h3><a href="EmpregadoController?action=novo-empregado">Adicionar Empregado</a></h3>
<font color=red><c:out value="${msgErro}" /></font>
<font color=green><c:out value="${msgSucesso}" /></font>
<br />
<br />
 <table border=1>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Departamento</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${empregados}" var="empregado">
                <tr>
                    <td><c:out value="${empregado.id}" /></td>
                    <td><c:out value="${empregado.nome}" /></td>
                    <td><c:out value="${empregado.departamentoNome}" /></td>
                    <td><a href="EmpregadoController?action=selecionar-empregado&id=<c:out value="${empregado.id}"/>">Atualizar</a></td>
                    <td><a href="EmpregadoController?action=excluir-empregado&id=<c:out value="${empregado.id}"/>">Excluir</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
    <h3><a href="index.jsp">Página Inicial</a></h3>
</body>
</html>