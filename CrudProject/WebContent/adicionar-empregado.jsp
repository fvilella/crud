<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<title>Adicionar Empregado</title>
</head>
<body>
 
<h3>Adicionar Empregado</h3>
<font color=red><c:out value="${msgErro}" /></font>
<br/>
<form action="EmpregadoController?action=adicionar-empregado" method="post">
 
	Nome: <input type="text" name="nome">
	<br>
	Departamento: <select name="departamentoId" width="20">
		              <c:forEach var="departamento" items="${departamentos}">
		                <option value="${departamento.id}">${departamento.nome}</option>
		              </c:forEach>
		            </select>
	<br>
	<input type="submit" value="Cadastro">
</form>
</body>
</html>