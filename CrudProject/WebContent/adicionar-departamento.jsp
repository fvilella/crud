<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<title>Adicionar Departamento</title>
</head>
<body>

<h3>Adicionar Departamento</h3>
<font color=red><c:out value="${msgErro}" /></font>
<br/>
<form action="DepartamentoController?action=adicionar-departamento" method="post">
 
	Nome: <input type="text" name="nome">
	<br>
	Descrição: <input type="text" name="descricao">
	<br>
	<input type="submit" value="Cadastro">
</form>
</body>
</html>