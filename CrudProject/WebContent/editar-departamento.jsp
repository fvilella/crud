<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<title>Atualizar Departamento</title>
</head>
<body>
<h3>Atualizar Departamento</h3>
<font color=red><c:out value="${msgErro}" /></font>
<br/>
<form action="DepartamentoController?action=editar-departamento" method="post">
 	<input type="hidden" name="id" value="<c:out value="${departamento.id}" />">
	Nome: <input type="text" name="nome" value="<c:out value="${departamento.nome}" />">
	<br>
	Descrição: <input type="text" name="descricao" value="<c:out value="${departamento.descricao}" />">
	<br>
	<input type="submit" value="Atualizar">
</form>
</body>
</html>