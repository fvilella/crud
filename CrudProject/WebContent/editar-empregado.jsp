<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<title>Atualizar Empregado</title>
</head>
<body>
<h3>Atualizar Empregado</h3>
<font color=red><c:out value="${msgErro}" /></font>
<br/>
<form action="EmpregadoController?action=editar-empregado" method="post">
 	<input type="hidden" name="id" value="<c:out value="${empregado.id}" />">
	Nome: <input type="text" name="nome" value="<c:out value="${empregado.nome}" />">
	<br>
	Descrição: <select name="departamentoId" width="20">
		              <c:forEach var="departamento" items="${departamentos}">
		                <option value="${departamento.id}">${departamento.nome}</option>
		              </c:forEach>
		            </select>
	<br>
	<input type="submit" value="Atualizar">
</form>
</body>
</html>