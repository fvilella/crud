<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Departamento</title>
</head>
<body>
 
<h1>Departamentos</h1>
<br />
<h3><a href="adicionar-departamento.jsp">Adicionar Departamento</a></h3>
<font color=red><c:out value="${msgErro}" /></font>
<font color=green><c:out value="${msgSucesso}" /></font>
<br />
<br />
 <table border=1>
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Descrição</th>
            <th colspan=2>Action</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${departamentos}" var="departamento">
            <tr>
                <td><c:out value="${departamento.id}" /></td>
                <td><c:out value="${departamento.nome}" /></td>
                <td><c:out value="${departamento.descricao}" /></td>
                <td><a href="DepartamentoController?action=selecionar-departamento&id=<c:out value="${departamento.id}"/>">Editar</a></td>
                <td><a href="DepartamentoController?action=excluir-departamento&id=<c:out value="${departamento.id}"/>">Excluir</a></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
    
<br />
<h4>Total de departamentos sem empregados: <c:out value="${departamentosVazios}" /></h4>

<h3><a href="index.jsp">Página Inicial</a></h3>
</body>
</html>