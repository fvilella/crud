package br.com.crud.project.facade;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import br.com.crud.project.bean.Departamento;
import br.com.crud.project.bean.Empregado;
import br.com.crud.project.util.DepartamentoUtil;
import br.com.crud.project.util.EmpregadoUtil;

public class CrudFacadeTest {
	
	Departamento departamento;
	Empregado empregado;
	
	
	@Test
    public void adicionarDepartamentoSemNomeTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		
		departamento = new Departamento();
		
		String exceptionMessage = "";
		try {
			crudFacade.adicionarDepartamento(departamento);
		} catch (Exception e) {
			exceptionMessage = e.getMessage();
		}

		Departamento ultimoDepartamento = crudFacade.retornaUltimoDepartamento();
		assertNotNull(ultimoDepartamento.getNome());
		assertNotNull(ultimoDepartamento.getDescricao());
		
		assertEquals(exceptionMessage, DepartamentoUtil.DEPARTAMENTO_INVALIDO);

	}
	
	@Test
    public void adicionarDepartamentoTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		
		departamento = new Departamento();
		departamento.setNome("Test Junit");
		departamento.setDescricao("Test descri��o");
		
		try {
			crudFacade.adicionarDepartamento(departamento);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Departamento ultimoDepartamento = crudFacade.retornaUltimoDepartamento();
		assertEquals(departamento.getNome(), ultimoDepartamento.getNome());
		assertEquals(departamento.getDescricao(), ultimoDepartamento.getDescricao());
	}
	
	@Test
    public void atualizarDepartamentoSemNomeTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		
		departamento = crudFacade.retornaUltimoDepartamento();

		if (departamento.getNome() == null) {
			 adicionarDepartamentoTest();
			 departamento = crudFacade.retornaUltimoDepartamento();
		}
		
		departamento.setNome(null);
		departamento.setDescricao("Test descri��o ATUALIZADO");
		
		String exceptionMessage = "";
		try {
			crudFacade.editarDepartamento(departamento);
		} catch (Exception e) {
			exceptionMessage = e.getMessage();
		}

		Departamento ultimoDepartamento = crudFacade.retornaUltimoDepartamento();
		
		assertNotNull(departamento.getNome(), ultimoDepartamento.getNome());
		assertNotNull(departamento.getDescricao(), ultimoDepartamento.getDescricao());

		assertEquals(exceptionMessage, DepartamentoUtil.DEPARTAMENTO_INVALIDO);
	}
	
	@Test
    public void atualizarDepartamentoTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		
		departamento = crudFacade.retornaUltimoDepartamento();

		if (departamento.getNome() == null) {
			 adicionarDepartamentoTest();
			 departamento = crudFacade.retornaUltimoDepartamento();
		}
		departamento.setNome("Test Junit ATUALIZADO");
		departamento.setDescricao("Test descri��o ATUALIZADO");
		
		try {
			crudFacade.editarDepartamento(departamento);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Departamento ultimoDepartamento = crudFacade.retornaUltimoDepartamento();
		
		assertEquals(departamento.getNome(), ultimoDepartamento.getNome());
		assertEquals(departamento.getDescricao(), ultimoDepartamento.getDescricao());

	}
	
	@Test
    public void excluirDepartamentoTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		
		departamento = new Departamento();
		departamento.setNome("Test Excluir");
		departamento.setDescricao("Test descri��o Excluir");
		
		try {
			crudFacade.adicionarDepartamento(departamento);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		departamento = crudFacade.retornaUltimoDepartamento();

		try {
			crudFacade.excluirDepartamento(departamento.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Departamento ultimoDepartamento = crudFacade.retornaUltimoDepartamento();
		
		String idAntigo = Integer.toString(departamento.getId());
		String idNovo = Integer.toString(ultimoDepartamento.getId());
		assertFalse(idAntigo.equals(idNovo));

	}
	
	@Test
    public void excluirDepartamentoUsadoTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		
		departamento = crudFacade.retornaUltimoDepartamento();
		
		empregado = new Empregado();
		empregado.setNome("Test Empregado");
		empregado.setDepartamentoId(departamento.getId());
		
		try {
			crudFacade.adicionarEmpregado(empregado);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String exceptionMessage = "";
		
		try {
			crudFacade.excluirDepartamento(departamento.getId());
		} catch (Exception e) {
			exceptionMessage = e.getMessage();
		}

		assertEquals(exceptionMessage, DepartamentoUtil.DEPARTAMENTO_USADO);

	}

	@Test
    public void adicionarEmpregadoTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		Departamento ultimoDepartamento = crudFacade.retornaUltimoDepartamento();
		
		empregado = new Empregado();
		empregado.setNome("Test Empregado");
		empregado.setDepartamentoId(ultimoDepartamento.getId());
		
		try {
			crudFacade.adicionarEmpregado(empregado);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Empregado ultimoEmpregado = crudFacade.retornaUltimoEmpregado();
		assertEquals(empregado.getNome(), ultimoEmpregado.getNome());
		assertEquals(empregado.getDepartamentoId(), ultimoEmpregado.getDepartamentoId());
	}
	
	@Test
    public void adicionarEmpregadoSemNomeTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		Departamento ultimoDepartamento = crudFacade.retornaUltimoDepartamento();
		
		empregado = new Empregado();
		empregado.setNome(null);
		empregado.setDepartamentoId(ultimoDepartamento.getId());
		
		String exceptionMessage = "";
		try {
			crudFacade.adicionarEmpregado(empregado);
		} catch (Exception e) {
			exceptionMessage = e.getMessage();
		}
		
		assertEquals(exceptionMessage, EmpregadoUtil.EMPREGADO_INVALIDO);
	}
	
	@Test
    public void atualizarEmpregadoTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		Departamento ultimoDepartamento = crudFacade.retornaUltimoDepartamento();
		
		empregado = crudFacade.retornaUltimoEmpregado();

		empregado.setNome("Test Empregado ATUALIZADO");
		empregado.setDepartamentoId(ultimoDepartamento.getId());
		
		try {
			crudFacade.editarEmpregado(empregado);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Empregado ultimoEmpregado = crudFacade.retornaUltimoEmpregado();
		
		assertEquals(empregado.getNome(), ultimoEmpregado.getNome());
		assertEquals(empregado.getDepartamentoId(), ultimoEmpregado.getDepartamentoId());

	}
	
	@Test
    public void atualizarEmpregadoSemNomeTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		Departamento ultimoDepartamento = crudFacade.retornaUltimoDepartamento();
		
		empregado = new Empregado();
		empregado.setNome(null);
		empregado.setDepartamentoId(ultimoDepartamento.getId());
		
		String exceptionMessage = "";
		try {
			crudFacade.editarEmpregado(empregado);
		} catch (Exception e) {
			exceptionMessage = e.getMessage();
		}
		
		assertEquals(exceptionMessage, EmpregadoUtil.EMPREGADO_INVALIDO);
	}
	
	@Test
    public void excluirEmpregadoTest() {
		CrudFacadeImpl crudFacade = new CrudFacadeImpl();
		
		empregado = crudFacade.retornaUltimoEmpregado();
		
		crudFacade.excluirEmpregado(empregado.getId());
		
		Empregado ultimoEmpregado = crudFacade.retornaUltimoEmpregado();
		
		String idAntigo = Integer.toString(empregado.getId());
		String idNovo = Integer.toString(ultimoEmpregado.getId());
		assertFalse(idAntigo.equals(idNovo));

	}

}

