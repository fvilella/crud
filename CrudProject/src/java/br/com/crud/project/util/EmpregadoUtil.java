package br.com.crud.project.util;

import br.com.crud.project.bean.Empregado;

public class EmpregadoUtil {
	
	public static final String EMPREGADO_INVALIDO = "Empregado Inv�lido";
	
	public static boolean validaEmpregado(Empregado empregado) {
		boolean isValid = false;
		
		if (empregado.getNome() != null){
			isValid = true;
		}
		
		return isValid;
	}
	
}
