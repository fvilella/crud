package br.com.crud.project.util;

import br.com.crud.project.bean.Departamento;

public class DepartamentoUtil {
	
	public static final String DEPARTAMENTO_INVALIDO = "Departamento Inv�lido";
	public static final String DEPARTAMENTO_USADO = "Departamento Usado";
	
	public static boolean validaDepartamento(Departamento departamento) {
		boolean isValid = false;
		
		if (departamento.getNome() != null){
			isValid = true;
		}
		
		return isValid;
	}
	
}
