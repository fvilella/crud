package br.com.crud.project.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mysql.jdbc.Statement;

public class DaoUtil {
	private static final Logger logger = LogManager.getLogger("DaoUtil");
	
	public static void close(Connection connection, PreparedStatement preparedStatement, Statement statement, ResultSet resultSet) {
		if (resultSet != null) {
		    try {
		        resultSet.close();
		    } catch (SQLException e) {
		    	logger.error(e.getMessage());
		        e.printStackTrace();
		    }
		}
		if (statement != null) {
		    try {
		        statement.close();
		    } catch (SQLException e) {
		    	logger.error(e.getMessage());
		        e.printStackTrace();
		    }
		}
		if (connection != null) {
		    try {
		        connection.close();
		    } catch (SQLException e) {
		    	logger.error(e.getMessage());
		        e.printStackTrace();
		    }
		}
	}
}
