package br.com.crud.project.util;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DatabaseConfig {
	 public static final String URL = "jdbc:mysql://localhost:3306/crud";
	 
	 private static final Logger logger = LogManager.getLogger("DatabaseConfig");
	 public static final String USERNAME = "root";
	 public static final String PASSWORD = "";
	 
	public static Connection getConnection() {
        Connection connection = null;
 
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (Exception e) {
        	logger.error(e.getMessage());
        }
 
        return connection;
    }
}
