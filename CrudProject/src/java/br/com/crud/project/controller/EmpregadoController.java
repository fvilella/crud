package br.com.crud.project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.crud.project.bean.Departamento;
import br.com.crud.project.bean.Empregado;
import br.com.crud.project.facade.CrudFacadeImpl;

@WebServlet("/EmpregadoController")
public class EmpregadoController extends HttpServlet {
	
	private static final Logger logger = LogManager.getLogger("EmpregadoController");
	
	private static final long serialVersionUID = -2888092060843547549L;
	
	private static final String ADICIONAR_SUCESSO = "Empregado cadastrado com sucesso";
	private static final String EDITAR_SUCESSO = "Empregado atualizado com sucesso";
	private static final String EXCLUIR_SUCESSO = "Empregado exclu�do com sucesso";
	private static final String ERRO = "Aconteceu um erro. Favor entrar em contato com o administrador, caso ocorra novamente";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String redirect = ""; 
        String nome = request.getParameter("nome");
        String departamentoIdStr = request.getParameter("departamentoId");
        final String action = request.getParameter("action");
        
        int departamentoId = 0;
        
        if (departamentoIdStr != null && !"".equals(departamentoIdStr)) {
        	departamentoId = Integer.valueOf(departamentoIdStr);
        	
        	CrudFacadeImpl crudFacadeImpl;
            
            Empregado empregado = new Empregado();
            empregado.setNome(nome);
            empregado.setDepartamentoId(departamentoId);

            if ("adicionar-empregado".equals(action)) {
                 try {
                 	crudFacadeImpl = new CrudFacadeImpl();
                 	crudFacadeImpl.adicionarEmpregado(empregado);	
    	        	redirect = "EmpregadoController?action=lista-empregados&msgSucesso=ADICIONAR_SUCESSO";
    	    		response.sendRedirect(redirect);
    			} catch (Exception e) {
    				request.setAttribute("msgErro", ERRO);
    				redirect="adicionar-empregado.jsp";
    				logger.error(e.getMessage());
    				RequestDispatcher view = getServletContext().getRequestDispatcher(redirect);
    				view.forward(request, response);
    			}
            } else if ("editar-empregado".equals(action)) {
    			final int id = Integer.valueOf(request.getParameter("id"));
    			
    			empregado.setId(id);
    			
    			try {
    				crudFacadeImpl = new CrudFacadeImpl();
                 	crudFacadeImpl.editarEmpregado(empregado);
        			redirect = "EmpregadoController?action=lista-empregados&msgSucesso=EDITAR_SUCESSO";
            		response.sendRedirect(redirect);
    			} catch (Exception e) {
    				request.setAttribute("msgErro", ERRO);
    				redirect="editar-empregado.jsp";
    				logger.error(e.getMessage());
    				RequestDispatcher view = getServletContext().getRequestDispatcher(redirect);
    				view.forward(request, response);
    			}

            }
    		

		} else {
			request.setAttribute("msgErro", "Favor selecione um departamento.");
			logger.info("Departamento n�o selecionado");
			 if ("adicionar-empregado".equals(action)) {
				 redirect="/adicionar-empregado.jsp";
            } else if ("editar-empregado".equals(action)) {
            	redirect="/editar-empregado.jsp";
            }
			
			RequestDispatcher view = getServletContext().getRequestDispatcher(redirect);
			view.forward(request, response);
		}

    }
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String redirect = "";
		
		final String action = request.getParameter("action");
		
		final String msgSucesso = (String) request.getParameter("msgSucesso");

		if (msgSucesso != null && !"".equals(msgSucesso)) {
			if (msgSucesso.equals("ADICIONAR_SUCESSO")) {
				request.setAttribute("msgSucesso", ADICIONAR_SUCESSO);
			} else if (msgSucesso.equals("EDITAR_SUCESSO")) {
				request.setAttribute("msgSucesso", EDITAR_SUCESSO);
			}
		}
		
		CrudFacadeImpl crudFacadeImpl;
		if ("novo-empregado".equals(action)) {
			 try {
			 	crudFacadeImpl = new CrudFacadeImpl();
	        	List<Departamento> lista = crudFacadeImpl.listarDepartamentos();
	        	if (lista!=null && !lista.isEmpty()) {
					request.setAttribute("departamentos", lista);
				}

			} catch (Exception e) {
				request.setAttribute("msgErro", ERRO);
				logger.error(e.getMessage());
			}
			redirect = "/adicionar-empregado.jsp";
		} else if ("lista-empregados".equals(action)) {
			 try {
			 	crudFacadeImpl = new CrudFacadeImpl();
	        	List<Empregado> lista = crudFacadeImpl.listarEmpregados();

	        	if (lista!=null && !lista.isEmpty()) {
					request.setAttribute("empregados", lista);
				}

			} catch (Exception e) {
				request.setAttribute("msgErro", ERRO);
				logger.error(e.getMessage());
			}
			redirect = "/empregados.jsp";
		} else if ("selecionar-empregado".equals(action)) {
			final int id = Integer.valueOf(request.getParameter("id"));
			try {
				crudFacadeImpl = new CrudFacadeImpl();
             	Empregado empregado = crudFacadeImpl.retornaEmpregado(id);
             	request.setAttribute("empregado", empregado);
             	
             	List<Departamento> lista = crudFacadeImpl.listarDepartamentos();

	        	if (lista!=null && !lista.isEmpty()) {
					request.setAttribute("departamentos", lista);
				}
	        	
			} catch (Exception e) {
				request.setAttribute("msgErro", ERRO);
				logger.error(e.getMessage());
			}
			redirect = "/editar-empregado.jsp";
        } else if ("excluir-empregado".equals(action)) {
			try {
				final int id = Integer.valueOf(request.getParameter("id"));
			 	crudFacadeImpl = new CrudFacadeImpl();
	        	crudFacadeImpl.excluirEmpregado(id);
	        	request.setAttribute("msgSucesso", EXCLUIR_SUCESSO);
			} catch (Exception e) {
				request.setAttribute("msgErro", ERRO);
				logger.error(e.getMessage());
			}
			redirect = "/EmpregadoController?action=lista-empregados";
		}
		
		RequestDispatcher view = getServletContext().getRequestDispatcher(redirect);
		view.forward(request, response);

	}

}