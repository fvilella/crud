package br.com.crud.project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.crud.project.bean.Departamento;
import br.com.crud.project.facade.CrudFacadeImpl;
import br.com.crud.project.util.DepartamentoUtil;

@WebServlet("/DepartamentoController")
public class DepartamentoController extends HttpServlet {
	
	private static final Logger logger = LogManager.getLogger("DepartamentoController");	

	private static final long serialVersionUID = -7803609738144987362L;
	
	private String msgSucesso;
	private static final String ADICIONAR_SUCESSO = "Departamento cadastrado com sucesso";
	private static final String EDITAR_SUCESSO = "Departamento atualizado com sucesso";
	private static final String EXCLUIR_SUCESSO = "Departamento exclu�do com sucesso";
	private static final String EXCLUIR_USADO_ERRO = "Desculpe, mas o departamento sendo utilizado e n�o pode ser exclu�do.";
	private static final String ERRO = "Aconteceu um erro. Favor entrar em contato com o administrador, caso ocorra novamente";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
        String nome = request.getParameter("nome");
        String descricao = request.getParameter("descricao");
         
        final String action = request.getParameter("action");
        
        CrudFacadeImpl crudFacadeImpl;

        Departamento departamento = new Departamento();
        departamento.setNome(nome);
        departamento.setDescricao(descricao);
        
        String redirect = "";
        if ("adicionar-departamento".equals(action)) {
             try {
             	crudFacadeImpl = new CrudFacadeImpl();
             	crudFacadeImpl.adicionarDepartamento(departamento);	
	        	redirect = "DepartamentoController?action=lista-departamentos&msgSucesso=ADICIONAR_SUCESSO";
			} catch (Exception e) {
				request.setAttribute("msgErro", ERRO);
				redirect="/atualizar-departamento.jsp";
				logger.error(e.getMessage());
			}
        } else if ("editar-departamento".equals(action)) {
			final int id = Integer.valueOf(request.getParameter("id"));
			
			departamento.setId(id);
			
			try {
				crudFacadeImpl = new CrudFacadeImpl();
             	crudFacadeImpl.editarDepartamento(departamento);	
			} catch (Exception e) {
				request.setAttribute("msgErro", ERRO);
				redirect="/atualizar-departamento.jsp";
				logger.error(e.getMessage());
			}
			redirect = "DepartamentoController?action=lista-departamentos&msgSucesso=EDITAR_SUCESSO";
        }
		
		request.setAttribute("msgSucesso", msgSucesso);

		response.sendRedirect(redirect);
        
    }
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String redirect = "";

		final String action = request.getParameter("action");
		final String msgSucesso = (String) request.getParameter("msgSucesso");

		if (msgSucesso != null && !"".equals(msgSucesso)) {
			if (msgSucesso.equals("ADICIONAR_SUCESSO")) {
				request.setAttribute("msgSucesso", ADICIONAR_SUCESSO);
			} else if (msgSucesso.equals("EDITAR_SUCESSO")) {
				request.setAttribute("msgSucesso", EDITAR_SUCESSO);
			}
		}
		
		CrudFacadeImpl crudFacadeImpl;
		if ("lista-departamentos".equals(action)) {
			 try {
			 	crudFacadeImpl = new CrudFacadeImpl();
	        	List<Departamento> lista = crudFacadeImpl.listarDepartamentos();

	        	if (lista!=null && !lista.isEmpty()) {
					request.setAttribute("departamentos", lista);
				}
	        	
	        	crudFacadeImpl = new CrudFacadeImpl();
	        	List<Departamento> departamentosVazios = crudFacadeImpl.listarDepartamentosVazios();

	        	if (departamentosVazios != null) {
					request.setAttribute("departamentosVazios", departamentosVazios.size());
				}
			} catch (Exception e) {
				request.setAttribute("msgErro", ERRO);
				logger.error(e.getMessage());
			}
			redirect = "/departamentos.jsp";
		} else if ("selecionar-departamento".equals(action)) {
			final int id = Integer.valueOf(request.getParameter("id"));
			try {
				crudFacadeImpl = new CrudFacadeImpl();
             	Departamento departamento = crudFacadeImpl.retornaDepartamento(id);
             	request.setAttribute("departamento", departamento);
			} catch (Exception e) {
				request.setAttribute("msgErro", ERRO);
				logger.error(e.getMessage());
			}
			redirect = "/editar-departamento.jsp";
        } else if ("excluir-departamento".equals(action)) {
			try {
				final int id = Integer.valueOf(request.getParameter("id"));
			 	crudFacadeImpl = new CrudFacadeImpl();

				crudFacadeImpl.excluirDepartamento(id);
				request.setAttribute("msgSucesso", EXCLUIR_SUCESSO);
			} catch (Exception e) {
				if (DepartamentoUtil.DEPARTAMENTO_USADO.equals(e.getMessage())) {
					request.setAttribute("msgErro", EXCLUIR_USADO_ERRO);
				} else {
					request.setAttribute("msgErro", ERRO);
					logger.error(e.getMessage());
				}
			}

			redirect = "/DepartamentoController?action=lista-departamentos";
		}
		
		RequestDispatcher view = getServletContext().getRequestDispatcher(redirect);
		view.forward(request, response);

	}

}