package br.com.crud.project.dao;

import java.util.List;

import br.com.crud.project.bean.Departamento;

public interface DepartamentoDAO {
	 
	void cadastro(Departamento departamento);
	
	List<Departamento> listarDepartamentos();
	
	List<Departamento> listarDepartamentosVazios();

	void excluir(int id);
	
	Departamento retornaDepartamento(int id);
	
	void editar(Departamento departamento);

}
