package br.com.crud.project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.crud.project.bean.Empregado;
import br.com.crud.project.util.DaoUtil;
import br.com.crud.project.util.DatabaseConfig;

import com.mysql.jdbc.Statement;

public class EmpregadoDAOImpl implements EmpregadoDAO {
	
	private static final Logger logger = LogManager.getLogger("DepartamentoDAOImpl");
	
	@Override
    public void cadastro(Empregado empregado) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DatabaseConfig.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO empregado (nome, id_departamento) VALUES (?, ?)");
            preparedStatement.setString(1, empregado.getNome());
            preparedStatement.setInt(2, empregado.getDepartamentoId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
           
        } finally {
        	DaoUtil.close(connection, preparedStatement, null, null);
        }
    }
	
	@Override
	public List<Empregado> listarEmpregados() {
	        List<Empregado> listaEmpregado = new ArrayList<Empregado>();
	        Connection connection = null;
	        Statement statement = null;
	        ResultSet resultSet = null;
	 
	        try {
	            connection = DatabaseConfig.getConnection();
	            statement = (Statement) connection.createStatement();
	            resultSet = statement.executeQuery("select e.id as id, e.nome as nome, e.id_departamento as id_departamento, d.nome as departamento  from empregado e, departamento d where e.id_departamento = d.id order by e.id asc");
	 
	            while (resultSet.next()) {
	            	Empregado empregado = new Empregado();
	            	empregado.setId(resultSet.getInt("id"));
	            	empregado.setNome(resultSet.getString("nome"));
	            	empregado.setDepartamentoId(Integer.valueOf(resultSet.getString("id_departamento")));
	            	empregado.setDepartamentoNome(resultSet.getString("departamento"));
	 
	            	listaEmpregado.add(empregado);
	            }
	 
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	        	DaoUtil.close(connection, null, statement, resultSet);
	        }
			return listaEmpregado;
	}
	
	
	@Override
    public void excluir(int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
 
        try {
            connection = DatabaseConfig.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM empregado WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
 
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
        	DaoUtil.close(connection, preparedStatement, null, null);
        }
    }
	
	@Override
	public boolean departamentoUtilizado(int id){
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	        boolean usado = false;
	 
	        try {
	            connection = DatabaseConfig.getConnection();
	            preparedStatement = connection.prepareStatement("SELECT * FROM empregado WHERE id_departamento = ?");
	            preparedStatement.setInt(1, id);
	            resultSet = preparedStatement.executeQuery();
	 
	            while (resultSet.next()) {
	            	usado = true;
	            }
	 
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	        	DaoUtil.close(connection, preparedStatement, null, resultSet);
	        }
	 
	        return usado;
	}
	
	@Override
	public Empregado retornaUltimoEmpregado(){
		Empregado empregado = new Empregado();
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	 
	        try {
	            connection = DatabaseConfig.getConnection();
	            preparedStatement = connection.prepareStatement("select * from empregado order by id desc limit 1");
	            resultSet = preparedStatement.executeQuery();
	 
	            while (resultSet.next()) {
	            	empregado.setId(resultSet.getInt("id"));
	            	empregado.setNome(resultSet.getString("nome"));
	            	empregado.setDepartamentoId(Integer.valueOf(resultSet.getString("id_departamento")));
	            }
	 
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	        	DaoUtil.close(connection, preparedStatement, null, resultSet);
	        }
	 
	        return empregado;
	}
	
	@Override
	public Empregado retornaEmpregado(int id){
		Empregado empregado = new Empregado();
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	 
	        try {
	            connection = DatabaseConfig.getConnection();
	            preparedStatement = connection.prepareStatement("SELECT * FROM empregado WHERE id = ?");
	            preparedStatement.setInt(1, id);
	            resultSet = preparedStatement.executeQuery();
	 
	            while (resultSet.next()) {
	            	empregado.setId(resultSet.getInt("id"));
	            	empregado.setNome(resultSet.getString("nome"));
	            	empregado.setDepartamentoId(Integer.valueOf(resultSet.getString("id_departamento")));
	            }
	 
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	            DaoUtil.close(connection, preparedStatement, null, resultSet);
	        }
	 
	        return empregado;
	}
	
	@Override
	public void editar(Empregado empregado){
		Connection connection = null;
        PreparedStatement preparedStatement = null;
        
        try {
            connection = DatabaseConfig.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE empregado SET nome = ?, id_departamento = ? WHERE id = ?");
 
            preparedStatement.setString(1, empregado.getNome());
            preparedStatement.setInt(2, empregado.getDepartamentoId());
            preparedStatement.setInt(3, empregado.getId());
            preparedStatement.executeUpdate();
 
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            DaoUtil.close(connection, preparedStatement, null, null);
        }
	}
	
}
