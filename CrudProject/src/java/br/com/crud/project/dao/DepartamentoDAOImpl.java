package br.com.crud.project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.crud.project.bean.Departamento;
import br.com.crud.project.util.DaoUtil;
import br.com.crud.project.util.DatabaseConfig;

import com.mysql.jdbc.Statement;

public class DepartamentoDAOImpl implements DepartamentoDAO {
	
	private static final Logger logger = LogManager.getLogger("DepartamentoDAOImpl");
	
	@Override
    public void cadastro(Departamento departamento) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
 
        try {
            connection = DatabaseConfig.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO departamento (nome, descricao) VALUES (?, ?)");
            preparedStatement.setString(1, departamento.getNome());
            preparedStatement.setString(2, departamento.getDescricao());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
        	DaoUtil.close(connection, preparedStatement, null, null);
        }
    }
	
	@Override
	public List<Departamento> listarDepartamentos() {
	        List<Departamento> listaDepartamento = new ArrayList<Departamento>();
	        Connection connection = null;
	        Statement statement = null;
	        ResultSet resultSet = null;
	 
	        try {
	            connection = DatabaseConfig.getConnection();
	            statement = (Statement) connection.createStatement();
	            resultSet = statement.executeQuery("select * from departamento order by id asc");
	 
	            while (resultSet.next()) {
	            	Departamento departamento = new Departamento();
	            	departamento.setId(resultSet.getInt("id"));
	            	departamento.setNome(resultSet.getString("nome"));
	            	departamento.setDescricao(resultSet.getString("descricao"));
	 
	            	listaDepartamento.add(departamento);
	            }
	 
	        } catch (SQLException e) {
	        	logger.error(e.getMessage());
	        } finally {
	        	DaoUtil.close(connection, null, statement, resultSet);
	        }
			return listaDepartamento;
	}
	
	@Override
    public void excluir(int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
 
        try {
            connection = DatabaseConfig.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM departamento WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
 
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
        	DaoUtil.close(connection, preparedStatement, null, null);
        }
    }
	
	
	public Departamento retornaDepartamento(int id){
		Departamento departamento = new Departamento();
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	 
	        try {
	            connection = DatabaseConfig.getConnection();
	            preparedStatement = connection.prepareStatement("SELECT * FROM departamento WHERE id = ?");
	            preparedStatement.setInt(1, id);
	            resultSet = preparedStatement.executeQuery();
	 
	            while (resultSet.next()) {
	            	departamento.setId(resultSet.getInt("id"));
	            	departamento.setNome(resultSet.getString("nome"));
	            	departamento.setDescricao(resultSet.getString("descricao"));
	            }
	 
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	        	DaoUtil.close(connection, preparedStatement, null, resultSet);
	        }
	 
	        return departamento;
	}
	
	public Departamento retornaUltimoDepartamento(){
		Departamento departamento = new Departamento();
	        Connection connection = null;
	        PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	 
	        try {
	            connection = DatabaseConfig.getConnection();
	            preparedStatement = connection.prepareStatement("select * from departamento order by id desc limit 1");
	            resultSet = preparedStatement.executeQuery();
	 
	            while (resultSet.next()) {
	            	departamento.setId(resultSet.getInt("id"));
	            	departamento.setNome(resultSet.getString("nome"));
	            	departamento.setDescricao(resultSet.getString("descricao"));
	            }
	 
	        } catch (SQLException e) {
	            logger.error(e.getMessage());
	        } finally {
	        	DaoUtil.close(connection, preparedStatement, null, resultSet);
	        }
	 
	        return departamento;
	}
	
	public void editar(Departamento departamento){
		Connection connection = null;
        PreparedStatement preparedStatement = null;
        
        try {
            connection = DatabaseConfig.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE departamento SET nome = ?, descricao = ? WHERE id = ?");
 
            preparedStatement.setString(1, departamento.getNome());
            preparedStatement.setString(2, departamento.getDescricao());
            preparedStatement.setInt(3, departamento.getId());
            preparedStatement.executeUpdate();
 
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
        	DaoUtil.close(connection, preparedStatement, null, null);
        }
	}
	
	@Override
	public List<Departamento> listarDepartamentosVazios() {
	        List<Departamento> listaDepartamento = new ArrayList<Departamento>();
	        Connection connection = null;
	        Statement statement = null;
	        ResultSet resultSet = null;
	 
	        try {
	            connection = DatabaseConfig.getConnection();
	            statement = (Statement) connection.createStatement();
	            resultSet = statement.executeQuery("select * from departamento WHERE id NOT IN  ((select id_departamento from empregado))");
	           
	            while (resultSet.next()) {
	            	Departamento departamento = new Departamento();
	            	departamento.setId(resultSet.getInt("id"));
	            	departamento.setNome(resultSet.getString("nome"));
	            	departamento.setDescricao(resultSet.getString("descricao"));
	 
	            	listaDepartamento.add(departamento);
	            }
	 
	        } catch (SQLException e) {
	        	logger.error(e.getMessage());
	        } finally {
	            DaoUtil.close(connection, null, statement, resultSet);
	        }
			return listaDepartamento;
	}

	
}
