package br.com.crud.project.dao;

import java.util.List;

import br.com.crud.project.bean.Empregado;

public interface EmpregadoDAO {

	List<Empregado> listarEmpregados();
	 
	void cadastro(Empregado empregado);

	void excluir(int id);

	void editar(Empregado empregado);

	Empregado retornaEmpregado(int id);

	Empregado retornaUltimoEmpregado();

	boolean departamentoUtilizado(int id);
	
}
