package br.com.crud.project.facade;

import java.util.List;

import br.com.crud.project.bean.Departamento;
import br.com.crud.project.bean.Empregado;
import br.com.crud.project.dao.DepartamentoDAOImpl;
import br.com.crud.project.dao.EmpregadoDAOImpl;
import br.com.crud.project.util.DepartamentoUtil;
import br.com.crud.project.util.EmpregadoUtil;

public class CrudFacadeImpl implements CrudFacade {

	@Override
    public void adicionarDepartamento(Departamento departamento) throws Exception {
    	DepartamentoDAOImpl dao = new DepartamentoDAOImpl();
    	
    	if (DepartamentoUtil.validaDepartamento(departamento)){
    		dao.cadastro(departamento);
    	} else {
    		throw new Exception(DepartamentoUtil.DEPARTAMENTO_INVALIDO);
    	}

    }
    
	@Override
    public void editarDepartamento(Departamento departamento) throws Exception{
    	DepartamentoDAOImpl dao = new DepartamentoDAOImpl();
    	if (DepartamentoUtil.validaDepartamento(departamento)){
    		dao.editar(departamento);
    	} else {
    		throw new Exception(DepartamentoUtil.DEPARTAMENTO_INVALIDO);
    	}
    }
    
	@Override
    public void excluirDepartamento(int id) throws Exception{
		EmpregadoDAOImpl daoEmp = new EmpregadoDAOImpl();	
		boolean usado = daoEmp.departamentoUtilizado(id);
		
		if (usado) {
			throw new Exception(DepartamentoUtil.DEPARTAMENTO_USADO);
		} else {
	    	DepartamentoDAOImpl dao = new DepartamentoDAOImpl();
			dao.excluir(id);
		}

    }
    
	@Override
    public Departamento retornaDepartamento(int id){
    	DepartamentoDAOImpl dao = new DepartamentoDAOImpl();	
   	 	return dao.retornaDepartamento(id);
    }
    
	@Override
    public Departamento retornaUltimoDepartamento(){
    	DepartamentoDAOImpl dao = new DepartamentoDAOImpl();	
   	 	return dao.retornaUltimoDepartamento();
    }
    
	@Override
    public List<Departamento> listarDepartamentos(){
   	 DepartamentoDAOImpl dao = new DepartamentoDAOImpl();	
   	 return dao.listarDepartamentos();
    }
    
	@Override
    public List<Departamento> listarDepartamentosVazios(){
    	 DepartamentoDAOImpl dao = new DepartamentoDAOImpl();	
       	 return dao.listarDepartamentosVazios();
    }
    
	@Override
    public void adicionarEmpregado(Empregado empregado) throws Exception{
    	EmpregadoDAOImpl dao = new EmpregadoDAOImpl();
    	
    	if (EmpregadoUtil.validaEmpregado(empregado)){
    		dao.cadastro(empregado);
    	} else {
    		throw new Exception(EmpregadoUtil.EMPREGADO_INVALIDO);
    	}

    }
    
	@Override
    public void editarEmpregado(Empregado empregado) throws Exception{
		EmpregadoDAOImpl dao = new EmpregadoDAOImpl();
    	if (EmpregadoUtil.validaEmpregado(empregado)){
    		dao.editar(empregado);
    	} else {
    		throw new Exception(EmpregadoUtil.EMPREGADO_INVALIDO);
    	}
    }
    
	@Override
    public void excluirEmpregado(int id){
    	EmpregadoDAOImpl dao = new EmpregadoDAOImpl();	
     	dao.excluir(id);
    }
    
	@Override
    public List<Empregado> listarEmpregados(){
    	EmpregadoDAOImpl dao = new EmpregadoDAOImpl();	
    	return dao.listarEmpregados();
    }
    
	@Override
    public boolean departamentoUtilizado(int id){
    	EmpregadoDAOImpl dao = new EmpregadoDAOImpl();	
    	return dao.departamentoUtilizado(id);
    }
	
	@Override
    public Empregado retornaEmpregado(int id){
    	EmpregadoDAOImpl dao = new EmpregadoDAOImpl();	
   	 	return dao.retornaEmpregado(id);
    }
    
	@Override
    public Empregado retornaUltimoEmpregado(){
    	EmpregadoDAOImpl dao = new EmpregadoDAOImpl();	
   	 	return dao.retornaUltimoEmpregado();
    }
    
}
