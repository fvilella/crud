package br.com.crud.project.facade;

import java.util.List;

import br.com.crud.project.bean.Departamento;
import br.com.crud.project.bean.Empregado;

public interface CrudFacade {

	public void adicionarDepartamento(Departamento departamento) throws Exception;
    
    public void editarDepartamento(Departamento departamento) throws Exception;
    
    public void excluirDepartamento(int id) throws Exception;
    
    public List<Departamento> listarDepartamentos();
    
    public Departamento retornaDepartamento(int id);

    public List<Departamento> listarDepartamentosVazios();
    
    public void adicionarEmpregado(Empregado empregado) throws Exception;
    
    public void editarEmpregado(Empregado empregado) throws Exception;
    
    public void excluirEmpregado(int id);
    
    public Departamento retornaUltimoDepartamento();
    
    public Empregado retornaUltimoEmpregado();
    
    public List<Empregado> listarEmpregados();
    
    public boolean departamentoUtilizado(int id);
    
    public Empregado retornaEmpregado(int id);
    
}

